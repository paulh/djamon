from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
from surlex.dj import surl
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )

urlpatterns += patterns('',
    url(r'^login/', 'monitor.views.login_user', name='login'),
    url(r'^logout/', 'monitor.views.logout_user', name='logout'),
    surl(r'^get-group/<group_id>/$', 'monitor.views.group_data', name='get_group'),
    surl(r'^get-site/<site_id>/$', 'monitor.views.site_data', name='get_site'),
    surl(r'^outages/$', 'monitor.views.outages', name='outages'),
    surl(r'^outage/<outage_id>/$', 'monitor.views.outage', name='outage'),
    surl(r'^outage-<code>/$', 'monitor.views.outage', name='outage_code'),
    surl(r'^outage/$', 'monitor.views.outage', name='current_outage'),
    url(r'^', 'monitor.views.home', name='home'),
)   





