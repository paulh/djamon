DjaMon
=====================

This system is used to monitor other websites.  Into the database add a list 
of URLs to check.

Then a cronjob needs to be set up to check these sites.  It will let you 
know when it finds a problem, and then again when a problem is fixed.  You can 
set it to alert you via twitter or email.  

If you use Twitter, you can set up twitter to send you a text whenever the 
system sends a tweet (which is very useful).


Dependencies
------------------------

You will need the twitter tools::

    pip install twitter
    
You will then need to add the following information to a local_settings.py 
file::
    
    # This will let you send emails through Gmail
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_PORT = 587
    EMAIL_HOST_USER = 'yourusername@gmail.com'
    EMAIL_HOST_PASSWORD = ''
    
    # Your TO_EMAIL is a list of addresses to send monitoring emails to
    # Your FROM_EMAIL is the address that sends the emails.  If using Gmail, you'll need to set this up as a delagate
    # Set TWITTER = False if you don't want to send tweets
    TO_EMAIL = []
    FROM_EMAIL = EMAIL_HOST_USER
    TWITTER = True
    
    # Add a list of URLs that you know are usually working. If they are all down, then we assume the internet is down and don't bother with the monitoring process
    SANDBOX_URLS = ('http://www.bbc.co.uk/', 'http://www.google.co.uk/') 
    
    # If it finds one of these codes, then it's working
    GOOD_CODES = (200,301,304)
    
    # These are your keys from twitter.  You'll need to authorise your site as an app to get these
    CONSUMER_KEY = ''
    CONSUMER_SECRET = ''
    OAUTH_TOKEN = ''
    OAUTH_SECRET = ''

Then, it should all work.  


Limitations
-------------------------
There are a number of limitations that you should be aware of before using this 
app

* There is a limit of one email address and one twitter feed for the entire app.
* It relies on cronjobs.  If you can't run cronjobs, it's not going to work
* It relies on the HTTP protocol.  If a script can't be called using HTTP(S), it can't be included here
* You can't check addresses that require authentication.
* Some limitations are imposed by the Django UrlField - there is a maximum length, and URLs are expected to include a TLD (http://google/ can't be added, but http://google.com can).

These are on my to-do list for future improvements.


Further Information
-------------------------

Documentation is online at https://djamon.readthedocs.org/

I would also like to think that 'djamon' could be pronounced "Sha-mone". In 
homage to Michael Jackson.

http://www.youtube.com/watch?v=wa21_6xUBtw




