.. _code:

Django MVC
============================

Models
*********************************

.. automodule:: monitor.models
    :members:


Views
************************************

.. automodule:: monitor.views
    :members:
    
Templatetags
************************************


``readable_time``
---------------------------------

.. automodule:: monitor.templatetags.readable_time
    :members:

``number_suffix``
---------------------------------

.. automodule:: monitor.templatetags.number_suffix
    :members:
        
``paginator``
---------------------------------

.. automodule:: monitor.templatetags.paginator
    :members:
    
    