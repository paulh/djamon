.. Monitoring System documentation documentation master file, created by
   sphinx-quickstart on Fri Feb  8 14:32:28 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Monitoring System documentation
======================================================

This system is used to monitor the current status of web-based resources.

In simple terms, you need to setup various URLs to test important elements of 
your site, such as the database, file server, etc etc.  Then point this system 
at each of those URLs, and it will notify you if something goes wrong.

The source code is hosted on Bitbucket at 
https://bitbucket.org/paulh/djamon/.


.. note::
    It relies on a cron job to look for the URLs.  At the moment it checks 
    every minute during work time, and every 5 minutes outside of work time.  It 
    only records an 'outage' when any one element is still not available after 
    3 attempts.


Contents:
------------------------------

.. toctree::
   :maxdepth: 3
   
   Code <code>   
   Scripts <scripts>
   Alerts <alerts>




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

