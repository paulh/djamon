.. _scripts:

Python Scripts
============================

Cron Job
*********************************

``crontab``
---------------------------------
The cron job needs to have a record in your cron tab to work.

To open your crontab (in Ubuntu), run::

    crontab -e
    
That opens an editor allowing you to edit your crontab.  At present, the 
following has been set::

    */1 9-16 * * 1-5 python ~/private_html/django/djamon/scripts/cronjob.py
    */5 0-8 * * 1-5 python ~/private_html/django/djamon/scripts/cronjob.py
    */5 17-23 * * 1-5 python ~/private_html/django/djamon/scripts/cronjob.py
    */5 * * * 0,6 python ~/private_html/django/djamon/scripts/cronjob.py

This ensures that ``cronjob.py`` is called by the server every minute 
Monday-Friday between 9.00a.m. and 4.59p.m., and every 5 minutes at all other 
times.


``cronjob.py``
---------------------------------

This script is called by the cron job.

.. automodule:: scripts.cronjob
    :members:


The cron job checks that the internet connection is working before checking 
your site(s).  This means that it won't record an outage if the problem might 
not be localised to your web pages.  You will need to add some information to 
settings.py to set this up:: 

    # Add a list of URLs that you know are usually working. If they are all down, then we assume the internet is down and don't bother with the monitoring process
    SANDBOX_URLS = ('http://www.bbc.co.uk/', 'http://www.google.co.uk/') 
    
    GOOD_CODES = (200,301,304)

The good codes are a list of HTTP responses that it deems as being 'up'.  The 
sandbox urls are a list of URLs that usually work.


``my_shortcuts``
************************************

.. automodule:: scripts.my_shortcuts
    :members:


``status_code``
************************************

.. automodule:: scripts.status_code
    :members:


``unique_slugify``
************************************

.. automodule:: scripts.unique_slugify
    :members:
