.. _alerts:

Alerts
============================

The Monitoring System  is setup to alert you when your site is unavailable.  
To do this, it sends out a message whenever the site has been down for more 
than three attempts.

The reason it waits for three attempts is simple.  9 times out of 10, it will 
right itself after one or two attempts, in which case you needn't have bothered 
looking for a solution.  If it's still down after 3 attempts it's likely that 
it's a more serious problem, and it's fairly likely that it will be alerting 
you to the problem quicker than any end-user could.

The system can send two types of alert - an email and a Tweet.  

Email Alerts
------------------------------
Some settings need to be added to your settings.py file for this system to work.

You will then need to add the following information::

    FROM_EMAIL = ''
    TO_EMAIL = []
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_PORT = 587
    EMAIL_HOST_USER = '@gmail.com'
    EMAIL_HOST_PASSWORD = ''

This sets up gmail as the mail host.  The ``TO_EMAIL`` list is a list of all of 
the email addresses that should get copies of alerts.



Twitter
------------------------------
There are two big benefits of using twitter.

The first is that it will notify your customers of outages in real time.

The second is that you can set it up to text you whenever that account tweets.  
This means that you get an SMS text message notifying you that the site is down 
and it doesn't cost you anything.  This is awesome.


To send messages to twitter, you will need to set up Django to work with 
Twitter.  First install the python twitter library::

    pip install twitter
    
Then set up an OAUTH consumer key/token thingy.  This links your twitter 
account to this app.  The following needs to be added to settings.py::
    
    TWITTER = True #False if you don't want to tweet
    CONSUMER_KEY = ''
    CONSUMER_SECRET = ''
    OAUTH_TOKEN = ''
    OAUTH_SECRET = ''

If you setup a twitter account, then go to the developer tools section of 
twitter, it talks you through setting this up.  It's not that hard.


