from django.db import models

class Group(models.Model):
    """
    Group sites by type
    """
    title = models.CharField(max_length=50)
    
    def __unicode__(self):
        return self.title
    
PRIORITY_CHOICES = ((3,'Low'),
                    (2,'Normal'),
                    (1,'High'),
                    (0,'Critical'))

class Site(models.Model):
    """
    Core information about a site
    """
    title = models.CharField(max_length=40)
    url = models.URLField()
    is_active = models.BooleanField(default = True)
    down_message = models.CharField(max_length = 140, default="There is a problem with the website.")
    last_seen = models.DateTimeField(null=True, blank=True, default=None)
    down_count = models.IntegerField(default = 0)
    group = models.ForeignKey(Group)
    priority = models.IntegerField(choices=PRIORITY_CHOICES, default=2)
    
    def __unicode__(self):
        return self.title
    
class SiteView(models.Model):
    """
    Records when it has been accessed and the code returned
    
    This will get big very quickly, so there will need to be a database purge 
    script written.  This shouldn't be done on save because the normal 
    monitoring system should do no extra work.
    
    Using a UUID rather than an auto-increment for the pk
    """
    uuid = models.CharField(max_length=48, primary_key=True)
    site = models.ForeignKey(Site)
    attempt_time = models.DateTimeField(auto_now_add = True)
    status_code = models.IntegerField()
    
    def __unicode__(self):
        return self.site.title + ' - ' + str(self.attempt_time)

class Outage(models.Model):
    """
    Records when an outage has taken place, including all the things that have 
    gone down and the dates when it was resolved etc
    """
    start_time = models.DateTimeField()
    content = models.TextField()
    end_time = models.DateTimeField(blank=True, null=True, default=None)
    seconds_down = models.IntegerField(blank=True, null=True, default=None)
    extra_information = models.TextField(blank=True, null=True, default=None)
    sites = models.ManyToManyField(Site, through='OutageSites')
    notification_sent = models.BooleanField(default = False)
    code = models.CharField(max_length=8)
    
    def __unicode__(self):
        return str(self.start_time)
    
    
class OutageSites(models.Model):
    """
    Records which sites are effected by the outage
    """
    site = models.ForeignKey(Site)
    outage = models.ForeignKey(Outage)  
    primary = models.BooleanField(default = False)
    start_time = models.DateTimeField()
    
    def __unicode__(self):
        return self.site.title
    
    