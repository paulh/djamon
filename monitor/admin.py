from django.contrib import admin
from models import *

class GroupAdmin(admin.ModelAdmin):
    pass

class SiteAdmin(admin.ModelAdmin):
    list_display = ('group','url', 'is_active', 'last_seen', 'down_count','priority')
    list_filter = ('is_active','group','down_count','priority')

class SiteViewAdmin(admin.ModelAdmin):
    pass


class OutageAdmin(admin.ModelAdmin):
    pass

class OutageSitesAdmin(admin.ModelAdmin):
    pass

admin.site.register(Group, GroupAdmin)
admin.site.register(Site, SiteAdmin)
admin.site.register(SiteView, SiteViewAdmin)
admin.site.register(Outage, OutageAdmin)
admin.site.register(OutageSites, OutageSitesAdmin)