from django.shortcuts import render_to_response ,get_object_or_404
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.db.models import Q
from django.template import RequestContext
from django.contrib.contenttypes.models import ContentType
from scripts.my_shortcuts import render_to_hxr_response, paginate
from models import *
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.contrib import messages

def login_user(request):
    """
    Logs the user in
    """
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                messages.add_message(request, messages.SUCCESS, 'Welcome back!')
                return HttpResponseRedirect(reverse('home'))
        
        messages.add_message(request, messages.ERROR, 'We could not log you in')
        return HttpResponseRedirect(reverse('login'))
    return render_to_response('login.html',
                              context_instance=RequestContext(request))
    

def logout_user(request):
    logout(request)
    messages.add_message(request, messages.INFO, 'You have logged out')
    return HttpResponseRedirect(reverse('login'))

    
def home(request):
    """
    Show monitoring results for all systems being monitored.
    
    This doesn't do any parsing, it just gets the latest information from the 
    database.  A seperate script does the db update work.
    """
    
    if request.user.is_authenticated():
        groups = Group.objects.all()
        error_count = Site.objects.filter(is_active = True, down_count__gte = 3).count()  
        return render_to_response('home.html',
                                  {'groups': groups},
                                  context_instance=RequestContext(request))
    else:
         return HttpResponseRedirect(reverse('login'))
    
@login_required
def group_data(request, group_id):
    """
    Returns a json object containing details for each site being monitored in 
    this particular group
    """
    group = Group.objects.get(pk = group_id)
    sites = Site.objects.filter(group = group,
                                is_active = True).order_by('priority')
                                
    warning_count = sites.filter(down_count__gte = 1).count()
    error_count = sites.filter(down_count__gte = 3).count()                            
                                
    return render_to_hxr_response('group.html',
                                  {'sites': sites,
                                   'warning_count': warning_count,
                                   'error_count': error_count},
                                   context_instance=RequestContext(request))

@login_required
def site_data(request, site_id):
    """
    Returns details of the last 10 requests to view this particular site as a 
    json object
    """
    site = Site.objects.get(pk = site_id)
    recent_views = SiteView.objects.filter(site = site).order_by('-attempt_time')[:10]
    good_codes = settings.GOOD_CODES
    return render_to_hxr_response('views.html',
                                  {'recent': recent_views,
                                   'good_codes': good_codes},
                                  context_instance=RequestContext(request))

@login_required
def outage(request, outage_id=None, code=None):
    """
    Shows details of an outage, or the current outage if there is one
    """
    if outage_id is not None:
        outage = get_object_or_404(Outage, pk = outage_id)
    elif code is not None:
        try:
            outage = Outage.objects.filter(code = code).order_by('-start_date')[0]
        except:
            outage = get_object_or_404(Outage, end_time = None)
    else:
        outage = get_object_or_404(Outage, end_time = None)
        
    
    return render_to_response('outage.html',
                              {'outage': outage},
                              context_instance=RequestContext(request))

@login_required
def outages(request):
    """
    Lists all outages recorded on the system.  You can click on each one to 
    view the details of it.
    """
    outage_list = Outage.objects.all().order_by('-start_time')
    outages = paginate(outage_list, 25, request.GET.get('page'))
    
    return render_to_response('outages.html',
                              {'outages': outages},
                              context_instance=RequestContext(request))


