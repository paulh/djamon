from django import template
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
register = template.Library()

@register.inclusion_tag("fragments/pagination.html")
def paginate(obj):
    """
    Takes an object ``obj`` and puts the pagination links on screen.
    
    This is an inclusion tag rather than just calling the fragment because it 
    is totally generic - it uses ``{{obj}}`` rather than the name of the 
    variable.
    """
    return {'obj': obj}



