from django import template
register = template.Library()


def readable_time(value):
    """ Pass it a time in seconds and it returns a human readable string showing 
        the hours, minutes and seconds
    """
    if value:
        hours, remainder = divmod(value, 3600)
        minutes, seconds = divmod(remainder, 60)
        
        if hours == 0:
            hours_str = ''
        elif hours == 1:
            hours_str = '1 hour, '
        else:
            hours_str = '%s hours, ' %(hours)
            
        if minutes == 1:
            min_str = '1 minute'
        else:
            min_str = '%s minutes' %(minutes)
        
        if seconds == 0:
            sec_str = ''
        elif seconds == 1:
            sec_str = ', 1 second'
        else:
            sec_str = ', %s seconds' %(seconds)
            
        return hours_str + min_str + sec_str
    else:
        return value



register.filter('readable_time', readable_time)
